package com.niccher.ire;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

public class Lista extends AppCompatActivity {
    StringBuffer sbf;
    String mas="Filista.txt",mal="FilistaEnc.txt" , cont="*-*-";
    int statperm=1;
    TextView shw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        shw= (TextView) findViewById(R.id.showedlist);

        Inits();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == statperm)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(Lista.this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Lista.this, "Unable To Read", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }

        if (requestCode == statperm)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(Lista.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Lista.this, "Unable To Write", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void Inits(){
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                File All=new File(Environment.getExternalStorageDirectory().getAbsolutePath());
                String maindirpath = All.toString();
                File maindir = new File(maindirpath);

                if (maindir.exists() && maindir.isDirectory()) {
                    File arr[] = maindir.listFiles();
                    displayDirectoryContents(maindir);
                    Log.e("\t -Meth - ","-----------------Init Class");
                    Sav();
                }else{
                    Log.e("\t -Meth - ","-----------------Init Class Fail");

                }
            }
        },3000);

    }

    private void displayDirectoryContents(File dir) {
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    Log.e(" -Dir - ",file.getCanonicalPath());
                    cont+=" -Dir - "+file.getCanonicalPath();
                    displayDirectoryContents(file);
                } else {
                    Log.e("\t -File- ",file.getCanonicalPath());
                    cont+="\t -File- "+file.getCanonicalPath();
                }
            }

        } catch (IOException e) {
            Log.e(" -Fault- ",e.getMessage());
        }

        //Log.e(" -Round One- "," -------------------------------    ");
    }

    private void Sav(){
        String bigd= cont;
        String rilbigd= Base64.encodeToString(bigd.toString().getBytes(), Base64.DEFAULT);

        FileOutputStream fos = null;

        try {
            fos = openFileOutput(mas, MODE_PRIVATE);
            fos.write(bigd.getBytes());

            fos = openFileOutput(mal, MODE_PRIVATE);
            fos.write(rilbigd.getBytes());

            File fi= Environment.getExternalStorageDirectory();
            File fi2=new File(fi.getAbsolutePath()+"/RealNigga");
            fi2.mkdirs();
            File fi3 =new File(fi2,"DumpMal.txt");
            File fi31 =new File(fi2,"DumpMas.txt");
            File fi4 = new File(getFilesDir() + "/" + mal);
            File fi41 = new File(getFilesDir() + "/" + mas);

            try {
                copy(fi4, fi3);
                copy(fi41, fi31);
            }catch (IOException e){
                Log.e(" -Copy Error- ",e.getMessage());
            }

            Log.e("Bytes "," Pasted ");

        } catch (FileNotFoundException e) {
            Log.e("Error 1  ",e.getMessage());
        } catch (IOException e) {
            Log.e("Error 2  ",e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log.e("Error 3  ",e.getMessage());
                }
            }
        }
    }

    private void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                Log.e("Copy done"," Doneeeeeeeeee");
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}
